VHostApp
========

This is an application for creating vhost entries.  It takes a domain name for it main argument and then creates a vhost file as well as an entry in the hosts file for the vhost domain

[![endorse](http://api.coderwall.com/davidduggins/endorsecount.png)](http://coderwall.com/davidduggins)
###Version 0.1 alpha ChangeLog 11/27/2012###

- added the main class
- added a main function to run the app
- created code for porcessing the argv.
- created the vhost block
- created the hosts line
- added file support to create new 
